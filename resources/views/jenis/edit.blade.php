@extends('layout.master')

@section('judul')
Edit jenis {{$jenis->nama}}
@endsection

@section('content')

<form action="/jenis/{{$jenis->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Jenis</label>
        <input type="text" name="nama" value="{{$jenis->nama}}" class="form-control">
      </div>
      @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection