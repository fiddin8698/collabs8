@extends('layout.master')

@section('judul')
List Jenis
@endsection

@section('content')

<a href="/jenis/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Jenis</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($jenis as $key=>$item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/jenis/{{$item->id}}" method="POST">
                        <a href="/jenis/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/jenis/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @method('delete')
                        @csrf
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
        @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
        @endforelse
    </tbody>
</table>

@endsection