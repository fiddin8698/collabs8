@extends('layout.master')

@section('judul')
Edit Buku {{$buku->judul}}
@endsection

@section('content')

<form action="/buku/{{$buku->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Judul</label>
        <input type="text" name="judul" class="form-control">
      </div>
      @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
          <label>Penulis</label>
          <input type="text" name="penulis" class="form-control">
        </div>
      @error('penulis')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>Sinopsis</label>
        <textarea name="sinopsis" class="form-control" cols="30" rows="10"></textarea>
      </div>
      @error('sinopsis')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
          <label>Jenis</label>
          <input type="text" name="jenis_id" class="form-control">
        </div>
      @error('jenis_id')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection