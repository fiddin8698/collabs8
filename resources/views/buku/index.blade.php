@extends('layout.master')

@section('judul')
List Buku
@endsection

@section('content')

<a href="/buku/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Judul</th>
        <th scope="col">Penulis</th>
        <th scope="col">Sinopsis</th>
        <th scope="col">Jenis</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($buku as $key=>$item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->judul}}</td>
                <td>{{$item->penulis}}</td>
                <td>{{$item->sinopsis}}</td>
                <td>{{$item->jenis}}</td>
                <td>
                    <form action="/buku/{{$item->id}}" method="POST">
                        <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/buku/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @method('delete')
                        @csrf
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
        @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
        @endforelse
    </tbody>
</table>

@endsection