@extends('layout.master')

@section('judul')
Form Buku
@endsection

@section('content')

<form action="/buku" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" value="{{$buku->judul}}" class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Penulis</label>
        <input type="text" name="penulis" value="{{$buku->penulis}}" class="form-control">
      </div>
    @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Sinopsis</label>
      <textarea name="sinopsis" class="form-control" cols="30" rows="10">value="{{$buku->sinopsis}}"</textarea>
    </div>
    @error('sinopsis')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Jenis</label>
        <select name="jenis_id" class="form-control" id="">
            <option value="">---Pilih Jenis Buku---</option>
            @foreach ($jenis as $item)
                <option value="{{$item->$id}}">{{$item->judul}}</option>
            @endforeach
        </select>
      </div>
    @error('jenis_id')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Cover</label>
        <input type="file" name="cover" class="form-control">
      </div>
      @error('cover')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection