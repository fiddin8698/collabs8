<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function () {
    return view('layout.master');
});

//CRUD jenis
Route::get('/jenis/create', 'JenisController@create');
Route::post('/jenis', 'JenisController@store');
Route::get('/jenis', 'JenisController@index');
Route::get('/jenis/{jenis_id}', 'JenisController@show');
Route::get('/jenis/{jenis_id}/edit', 'JenisController@edit');
Route::put('/jenis/{jenis_id}', 'JenisController@update');
Route::delete('/jenis/{jenis_id}', 'JenisController@destroy');

//CRUD Buku
Route::resource('buku', 'BukuController');
