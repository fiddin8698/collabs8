<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class JenisController extends Controller
{
    public function create(){
        return view('jenis.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
        ]);

        DB::table('jenis')->insert([
            'nama' => $request['nama'],
        ]);

        return redirect('/jenis');
    }

    public function index(){
        $jenis = DB::table('jenis')->get();
        return view('jenis.index', compact('jenis'));
    }

    public function show($id){
        $jenis = DB::table('jenis')->where('id', $id)->first();
        return view('jenis.show', compact('jenis'));
    }

    public function edit($id){
        $jenis = DB::table('jenis')->where('id', $id)->first();
        return view('jenis.edit', compact('jenis'));
    }


    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
        ]);

        $query = DB::table('jenis')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                ]);
        return redirect('/jenis');
    }

    public function destroy($id){
        DB::table('jenis')->where('id', $id)->delete();
        return redirect('/jenis');
    }
}
